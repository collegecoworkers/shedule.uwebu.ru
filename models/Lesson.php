<?php

namespace app\models;

use Yii;
use yii\data\Pagination;
use yii\helpers\ArrayHelper;

class Lesson extends \yii\db\ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'lesson';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['title'], 'required'],
			[['title','time',], 'string'],
			[['title'], 'string', 'max' => 255],
			[['day_id', 'group_id'], 'number'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'title' => 'Название',
			'time' => 'Время',
			'day_id' => 'День',
			'group_id' => 'Группа',
		];
	}

	public function saveLesson()
	{

		$this->user_id = Yii::$app->user->id;
		if(!$this->date) $this->date = date('Y-m-d');

		return $this->save(false);
	}

	public function getGroup()
	{
		return Group::find()->where(['id' => $this->category_id])->one();
	}

	public static function getAll($pageSize = 5)
	{
		// build a DB query to get all lessons
		$query = Lesson::find();

		// get the total number of lessons (but do not fetch the lesson data yet)
		$count = $query->count();

		// create a pagination object with the total count
		$pagination = new Pagination(['totalCount' => $count, 'pageSize'=>$pageSize]);

		// limit the query using the pagination and retrieve the lessons
		$lessons = $query->offset($pagination->offset)
			->limit($pagination->limit)
			->all();
		
		$data['lessons'] = $lessons;
		$data['pagination'] = $pagination;
		
		return $data;
	}
}
