<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class PublicAsset extends AssetBundle
{
	public $basePath = '@webroot';
	public $baseUrl = '@web';
	
	public $css = [
		'public/vendor/bootstrap/css/bootstrap.min.css',
		'public/vendor/font-awesome/css/font-awesome.min.css',
		'public/css/font.css',
		'https://fonts.googleapis.com/css?family=Muli:300,400,700',
		'public/css/style.default.css',
		'public/css/custom.css',
	];
	
	public $js = [
    'https://code.jquery.com/jquery-3.2.1.min.js',
    'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js',
    'public/vendor/bootstrap/js/bootstrap.min.js',
    'public/vendor/jquery.cookie/jquery.cookie.js',
    'public/vendor/chart.js/Chart.min.js',
    'public/js/front.js',
	];

	public $depends = [
	];
}
