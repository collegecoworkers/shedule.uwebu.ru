<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\assets\PublicAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use app\models\Category;

PublicAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
	<meta charset="<?= Yii::$app->charset ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<?= Html::csrfMetaTags() ?>
	<title><?= Html::encode($this->title) ?></title>
	<?php $this->head() ?>

</head>
<body class="sticky_footer">
<?php $this->beginBody() ?>

	<?= $this->render('/partials/header');?>

	<div class="d-flex align-items-stretch">
		<?= $this->render('/partials/sidebar');?>
		<div class="page-content">
		<?= $content ?>
		<?= $this->render('/partials/footer');?>
		</div>
	</div>


<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
