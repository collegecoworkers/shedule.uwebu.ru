<?php

namespace app\models;

use Yii;
use yii\data\Pagination;

/**
 * This is the model class for table "day".
 *
 * @property integer $id
 * @property string $title
 */
class Day extends \yii\db\ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'day';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['title'], 'string', 'max' => 255],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'title' => 'Title',
		];
	}

	public function getLessons()
	{
		return $this->hasMany(Lesson::className(), ['day_id' => 'id']);
	}

	public function getLessonsCount()
	{
		return $this->getLessons()->count();
	}
	
	public static function getAll($param = null)
	{
		if($param){
			// die;
			return Day::find()->where($param)->all();
		} else{
			return Day::find()->all();
		}
	}
	
	public static function getLessonsByDay($id)
	{
		// build a DB query to get all products
		$query = Lesson::find()->where(['day_id'=>$id]);

		// get the total number of products (but do not fetch the product data yet)
		$count = $query->count();

		// create a pagination object with the total count
		$pagination = new Pagination(['totalCount' => $count, 'pageSize'=>6]);

		// limit the query using the pagination and retrieve the products
		$products = $query->offset($pagination->offset)
			->limit($pagination->limit)
			->all();

		$data['products'] = $products;
		$data['pagination'] = $pagination;
		
		return $data;
	}
}
