<?php

use yii\db\Migration;

/**
 * Handles the creation of table `day`.
 */
class m170124_021601_create_day_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('day', [
            'id' => $this->primaryKey(),
            'title'=>$this->string()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('day');
    }
}
