<?php

use yii\helpers\Html;
use yii\grid\GridView;

$this->title = 'Выберите день';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="page-header no-margin-bottom">
  <div class="container-fluid">
    <h2 class="h5 no-margin-bottom"><?= Html::encode($this->title) ?></h2>
  </div>
</div>

<ul class="breadcrumb">
  <div class="container-fluid">
  </div>
</ul>

<section class="no-padding-top">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12">
        <div class="block margin-bottom-sm">
          <div class="title"><strong><?= Html::encode($this->title) ?></strong></div>
          <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
              'title',
              [
                'label'=>'Действия', 
                'headerOptions' => ['width' => '100'],
                'format' => 'raw',
                'value' => function($data){
                  return Html::a('Перейти', ['site/day', 'group'=>$_GET['id'], 'id'=>$data->id ]);
                },
              ],
            ],
            ]); ?>
          </div>
        </div>
      </div>
    </div>
  </section>