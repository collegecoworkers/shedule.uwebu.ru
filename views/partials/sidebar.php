<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Group;
use app\models\User;

$groups = [];
$all_groups = Group::find()->all();

for ($i=0; $i < count($all_groups); $i++) {
	$item = $all_groups[$i];
	$groups[] = [
		'label' => $item->title,
		'url' => "/site/group?id=$item->id",
		'active' => ($this->context->route == 'site/group' and $_GET['id'] == $item->id)
	];
}

?>

<nav id="sidebar">
	<span class="heading">Панель</span>
	<ul class="list-unstyled">
		<li><a href="/"><i class="icon-home"></i>Главная</a></li>
		<li><a href="#dashvariants" aria-expanded="false" data-toggle="collapse"> <i class="icon-windows"></i>Группы</a>
			<ul id="dashvariants" class="collapse list-unstyled">
				<?php foreach($groups as $item): ?>
					<li class=" <?= (($item['active'] == 1) ? 'active' : '') ?>"><a href="<?= $item['url'] ?>"><?= $item['label'] ?></a></li>
				<?php endforeach ?>
				<?php if (User::isAdmin()): ?>
				<li class="<?= ($this->context->route == 'site/add-group') ? 'active' : '' ?>"><a href="/site/add-group"><i class="fa fa-plus"></i> Добавить группу</a></li>
				<?php endif ?>
			</ul>
		</li>
	</ul>
</nav>