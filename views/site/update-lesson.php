<?php 

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = Yii::t('app', 'Новая пара');
?>

<br>
<br>
<section class="no-padding-top">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12">
        <div class="block">
          <div class="title"><strong class="d-block">Обновить пару</strong></div>
          <div class="block-body">
            <?php $form = ActiveForm::begin(); ?>

            <?= $form->field($model, 'title')->textInput() ?>
            <?= $form->field($model, 'time')->textInput() ?>

            <div class="form-group center">
              <?= Html::submitButton('Обновить', ['class' => 'btn btn-success']) ?>
            </div>
            <?php ActiveForm::end(); ?>

          </div>
        </div>
      </div>
    </div>
  </div>
</section>