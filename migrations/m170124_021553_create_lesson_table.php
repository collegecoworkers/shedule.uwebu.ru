<?php

use yii\db\Migration;
// error 
// don't migrate
/**
 * Handles the creation of table `lesson`.
 */
class m170124_021553_create_lesson_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('lesson', [
            'id' => $this->primaryKey(),
            'title'=>$this->string(),
            'time'=>$this->time(),
            'day_id'=>$this->integer(),
            'group_id'=>$this->integer(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('lesson');
    }
}
