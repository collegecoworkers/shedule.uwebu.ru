<?php 
use app\models\User;
?>
<header class="header">
   <nav class="navbar navbar-expand-lg">
      <div class="container-fluid d-flex align-items-center justify-content-between">
         <div class="navbar-header">
            <a href="index.html" class="navbar-brand">
               <?php if(User::isAdmin()): ?>
                  <div class="brand-text brand-big visible text-uppercase"><strong class="text-primary">Админ</strong><strong> панель</strong></div>
                  <div class="brand-text brand-sm"><strong class="text-primary">А</strong><strong>П</strong></div>
               <?php else: ?>
                  <div class="brand-text brand-big visible text-uppercase"><strong class="text-primary">Панель</strong><strong>студента</strong></div>
                  <div class="brand-text brand-sm"><strong class="text-primary">П</strong><strong>С</strong></div>
               <?php endif ?>
            </a>
            <button class="sidebar-toggle"><i class="fa fa-long-arrow-left"></i></button>
         </div>
         <ul class="right-menu list-inline no-margin-bottom">
            <li class="list-inline-item logout"><a id="logout" href="/auth/logout" class="nav-link">Выход есть всегда <i class="icon-logout"></i></a></li>
         </ul>
      </div>
   </nav>
</header>