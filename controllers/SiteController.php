<?php

namespace app\controllers;

use Yii;
use yii\data\Pagination;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\data\ActiveDataProvider;
use yii\filters\VerbFilter;

use app\models\LoginForm;
use app\models\ContactForm;

use app\models\User;
use app\models\Group;
use app\models\Day;
use app\models\Lesson;

class SiteController extends Controller {

	public function init()
	{
		parent::init();
		if (Yii::$app->user->isGuest) {
			return $this->redirect('/auth/login');
		}
	}

	public function behaviors() {
		return [
		];
	}

	public function actions() {
		return [
			'error' => [
				'class' => 'yii\web\ErrorAction',
			],
			'captcha' => [
				'class' => 'yii\captcha\CaptchaAction',
				'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
			],
		];
	}

	public function actionIndex() {
		if (Yii::$app->user->isGuest) {
			return $this->redirect('/auth/login');
		} else {
			return $this->redirect('/site/main');
		}
	}

	public function actionMain() {

		$dataProvider = new ActiveDataProvider([ 'query' => Group::find() ]);

		return $this->render('index', [
			'dataProvider' => $dataProvider,
		]);
	}

	public function actionAddGroup()
	{
		$model = new Group();

		if ($model->load(Yii::$app->request->post())) {
			// $model->title = Yii::$app->request->post()['Group']['title'];
			$model->save();
			return $this->redirect(['index']);
		} else {
			return $this->render('add-group', [
				'model' => $model,
			]);
		}
	}

	public function actionUpdateGroup($id)
	{
		$model = Group::findOne($id);

		if ($model->load(Yii::$app->request->post())) {
			$model->save();
			return $this->redirect(['index']);
		} else {
			return $this->render('update-group', [
				'model' => $model,
			]);
		}
	}

	public function actionDeleteGroup($id)
	{
		Group::findOne($id)->delete();

		return $this->redirect(['index']);
	}

	public function actionAddLesson($group, $day)
	{
		$model = new Lesson();

		if ($model->load(Yii::$app->request->post())) {
			$model->group_id = $group;
			$model->day_id = $day;
			$model->save();
			return $this->redirect(['day', 'group' => $group, 'id' => $day]);
		} else {
			return $this->render('add-lesson', [
				'model' => $model,
			]);
		}
	}

	public function actionUpdateLesson($id)
	{
		$model = Lesson::findOne($id);

		if ($model->load(Yii::$app->request->post())) {
			$model->save();
			return $this->redirect(['group', 'id' => $model->group_id]);
		} else {
			return $this->render('update-lesson', [
				'model' => $model,
			]);
		}
	}

	public function actionDeleteLesson($id)
	{
		Lesson::findOne($id)->delete();

		return $this->redirect(['index']);
	}

	public function actionGroup($id)
	{
		return $this->render('group', [
			'dataProvider' => $this->getDays(),
		]);
	}

	public function actionDay($group, $id)
	{
		$dataProvider = new ActiveDataProvider([ 'query' => Lesson::find()->where(['group_id'=>$group, 'day_id'=>$id]) ]);
		return $this->render('lessons', [
			'day' => Day::findOne($id)->title,
			'dataProvider' => $dataProvider,
		]);
	}

	private function getDaysArray(){
		$days = [];
		$all_days = Day::find()->all();

		for ($i=0; $i < count($all_days); $i++) {
			$item = $all_days[$i];
			$days[$item->id] = $item->title;
		}
		return $days;
	}

	private function getDays(){
		return new ActiveDataProvider([ 'query' => Day::find() ]);
	}

}
