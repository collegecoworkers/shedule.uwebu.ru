<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\User;

$this->title = 'Пары на ' . $day;
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="page-header no-margin-bottom">
  <div class="container-fluid">
    <h2 class="h5 no-margin-bottom"><?= Html::encode($this->title) ?></h2>
  </div>
</div>

<ul class="breadcrumb">
  <div class="container-fluid">
  </div>
</ul>

<section class="no-padding-top">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12">
        <div class="block margin-bottom-sm">
        <?php if(User::isAdmin()): ?>
          <?= Html::a('Добавить пару', ['site/add-lesson', 'group'=> $_GET['group'], 'day'=> $_GET['id'] ]);  ?>
        <?php endif ?>
          <div class="title"><strong><?= Html::encode($this->title) ?></strong></div>
          <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
              'time',
              'title',
              [
                'class' => 'yii\grid\ActionColumn',
                'header'=>'Действия', 
                'visible' => (int)(User::isAdmin()),
                'headerOptions' => ['width' => '80'],
                'template' => '{update} {delete}',
                'buttons' => [
                  'update' => function ($url, $model) {
                    return Html::a('<span class="fa fa-pencil"></span>', ['site/update-lesson', 'id'=>$model->id ], [
                      'title' => Yii::t('app', 'lead-update'),
                    ]);
                  },
                  'delete' => function ($url, $model) {
                    return Html::a('<span class="fa fa-trash"></span>', ['site/delete-lesson', 'id'=>$model->id ], [
                      'title' => Yii::t('app', 'lead-delete'),
                    ]);
                  }
                ],
              ],
            ],
            ]); ?>
          </div>
        </div>
      </div>
    </div>
  </section>