<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Войти';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="login-page">
	<div class="container d-flex align-items-center">
		<div class="form-holder has-shadow">
			<div class="row">
				<!-- Logo & Information Panel-->
				<div class="col-lg-6">
					<div class="info d-flex align-items-center">
						<div class="content">
							<div class="logo">
								<h1>Cистема оповещений учебных занятий</h1>
							</div>
						</div>
					</div>
				</div>
				<!-- Form Panel    -->
				<div class="col-lg-6 bg-white">
					<div class="form d-flex align-items-center">
						<div class="content">
							<?php $form = ActiveForm::begin([
								'id' => 'login-form',
							]);
							?>
							<div class="form-group">
								<?= $form->field($model, 'email', [ 'template' => "{input}{label}", 'labelOptions' => [ 'class' => 'label-material']])->textInput(['autofocus' => true, 'class' => "input-material"]) ?>
							</div>
							<div class="form-group">
								<?= $form->field($model, 'password', [ 'template' => "{input}{label}", 'labelOptions' => [ 'class' => 'label-material']])->passwordInput(['autofocus' => true, 'class' => "input-material"]) ?>
							</div>
							<?= Html::submitButton('Войти', ['class' => 'btn btn-primary' ]) ?>
							<?php ActiveForm::end(); ?>
							<?= Html::a('Зарегистрироваться', ['auth/signup'], ['class' => 'signup']) ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="copyrights text-center">
		<a href="" class="external">Правая копия</a>
	</div>
</div>